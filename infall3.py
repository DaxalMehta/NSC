import numpy as np
import matplotlib.pyplot as plt
import classes
cl=classes.initial()
def radius(mass): # mass in msun
	slope,k=1.2,0.14
	mg=mass/1.E11
	c1=2.**(1./(3.-slope))-1.
	Rg=2.37 * c1 * (mg)**k
	R=31.26 * Rg * mg**0.167
	return Rg,R
def number(gmass,gcmass,Rgc):
	Rg=radius(gmass)[0]
	slope=1.2
	N=0.01*(gmass/gcmass)*(Rgc/(Rgc+Rg))**(3-slope)
	return N
def mass(gmass): # r in Kpc, gmass in msun
	Rg,R=radius(gmass)
	r=0.01
	slope=1.2
	mass = (gmass * (r/(r + Rg))**(3.0-slope))/(3.0-slope)
	return mass
def infall(gmass,gcmass):
	mg=gmass/1.E11
	mgc=gcmass/1.E11	

	a1,a2,a3 = 2.63,2.26,0.9
	slope,k,eccen=1.2,0.14,0.5
	alpha,beta=-0.67,1.76

	gfact=(2.-slope) * ((a1 * pow(1./(2.-slope), a2) + a3) * (1.-eccen) + eccen)
	f=0.3 * gfact * pow(2.37,1.5)*(2.**(1./(3.-slope))-1.0)**(1.5)
	rateg=0.01*2.0**(-3. + slope)/f * mg**(1.5*(1.-k) + alpha) * mgc**(-1.-alpha)
	rateh=(2.0**(2.0-slope)) * (2.0**(1.0/(3.0-slope))-1.0)**beta*rateg
	return rateg,rateh

def timescales(gmass,gcmass):
	Rg,r=radius(gmass)
	rateg,rateh=infall(gmass,gcmass)
	time=rateg**-1.0
	mg=gmass/1.E11
	mgc=gcmass/1.E11
	tburn =time * mg/mgc * 0.01 * pow(2.,-3.+cl.den)
	
	a1,a2,a3 = 2.63,2.26,0.9
	slope,k,eccen=1.2,0.14,0.5
	alpha,beta=-0.67,1.76	
	#rrh= Rg/(pow(2.,1./(3.-slope))-1.)

	gfact = (2-slope)*((a1*pow(1./(2.-slope), a2) + a3)*(1.-eccen) + eccen)
	tdf   = 0.3*(Rg**3/mg)**0.5* gfact * (mgc/mg)**alpha * (Rg/Rg)**beta
	return time, tburn, tdf

def infall2(gmass,gcmass,time):
	mg=gmass/1.E11
	mgc=gcmass/1.E11	
	Rg,R=radius(gmass)
	a1,a2,a3 = 2.63,2.26,0.9
	slope,k,eccen=1.2,0.14,0.5
	alpha,beta=-0.67,1.76
	
	rgc=Rg*(time/timescales(gmass,gcmass)[1])**(1/beta)
	
	gfact=(2.-slope) * ((a1 * pow(1./(2.-slope), a2) + a3) * (1.-eccen) + eccen)
	f=0.3 * gfact * pow(2.37,1.5)*(2.**(1./(3.-slope))-1.0)**(1.5)
	rateg=0.01*(rgc/(rgc+Rg))**(3. - slope)/f * mg**(1.5*(1.-k) + alpha) * mgc**(-1.-alpha)*(rgc/Rg)**(-beta)
	rateh=(2.0**(2.0-slope)) * (2.0**(1.0/(3.0-slope))-1.0)**beta * rateg
	return rateg,rateh

def infall3(gmass,gcmass,rgc):
	mg=gmass/1.E11
	mgc=gcmass/1.E11	
	Rg,R=radius(gmass)
	a1,a2,a3 = 2.63,2.26,0.9
	slope,k,eccen=1.2,0.14,0.5
	alpha,beta=-0.67,1.76
	
	#rgc=Rg*(time/timescales(gmass,gcmass)[1])**(-1/beta)
	
	gfact=(2.-slope) * ((a1 * pow(1./(2.-slope), a2) + a3) * (1.-eccen) + eccen)
	f=0.3 * gfact * pow(2.37,1.5)*(2.**(1./(3.-slope))-1.0)**(1.5)
	rateg=0.01*(rgc/(rgc+Rg))**(3. - slope)/f * mg**(1.5*(1.-k) + alpha) * mgc**(-1.-alpha)*(rgc/Rg)**(-beta)
	rateh=(2.0**(2.0-slope)) * (2.0**(1.0/(3.0-slope))-1.0)**beta * rateg
	return rateg,rateh

def timescales2(gmass,gcmass,rgc):
	Rg,r=radius(gmass)
	rateg,rateh=infall3(gmass,gcmass,rgc)
	time=rateg**-1.0
	mg=gmass/1.E11
	mgc=gcmass/1.E11
	
	tburn =time * mg/mgc * 0.01 * pow(rgc/(rgc+Rg),3.-cl.den)
	a1,a2,a3 = 2.63,2.26,0.9
	slope,k,eccen=1.2,0.14,0.5
	alpha,beta=-0.67,1.76	
	rrh= Rg/(pow(2.,1./(3.-slope))-1.)
	gfact = (2-slope)*((a1*pow(1./(2.-slope), a2) + a3)*(1.-eccen) + eccen)	
	tdf   = 0.3*(Rg**3/mg)**0.5* gfact * (mgc/mg)**alpha * (rgc/Rg)**beta
	return time, tburn, tdf

def halfmass_rad(gal_type,mass):
	if gal_type=="Early":
		c1,c2=6.27,1.95E6
		alpha,beta=0.347,-0.024
		reff=c1 * 10**(alpha * np.log10(mass/c2) + beta)
	elif gal_type=="Late":
		c1,c2=3.31,3.65E6
		alpha,beta=0.321,-0.011
		reff=c1 * 10.**(alpha * np.log10(mass/c2) + beta)
	rhm=4/3*reff
	return rhm,reff
def nsc_prop(gal_type,gmass,gcmass,time):
	assert isinstance(gmass, (float,int))==True, "Please insert one mass at a time"
	tHubble=13.6e3
	t=min(tHubble,timescales(gmass,gcmass)[1])
	time=np.where(time<t,time,t)
	
	### NSC Mass Calculation ###
	init_mass=mass(gmass)  			
	infall_rate=infall(gmass,gcmass)[1] 
	acc_rate=infall_rate*gcmass 		
	NSC_mass=init_mass+acc_rate*time
	m=np.random.normal(np.log10(NSC_mass),scale=0.15)
	NSC_mass=10**m 	
	
	### Half-Mass Radius ###
	rhm=halfmass_rad(gal_type,NSC_mass)[0] 	

	### Density###
	density=(3. * NSC_mass)/(4. * np.pi * rhm**3)
	
	### Escape Velocity ###
	G,msun,pc=6.67E-11,2.E30,3.086E16
	v_esc=np.sqrt(0.4*NSC_mass * msun *G/(rhm * pc)) * 100
	disp=v_esc/2
	
	return init_mass, NSC_mass, rhm, v_esc, density#,disp
def nsc_prop2(gal_type,gmass,gcmass,time):
	assert isinstance(gmass, (float,int))==True, "Please insert one mass at a time"
	tHubble=13.6e3
	t=min(tHubble,timescales(gmass,gcmass)[1])
	time=np.where(time<t,time,t)
	
	### NSC Mass Calculation ###
	init_mass=mass(gmass)  			
	#infall_rate=infall(gmass,gcmass)[1] 
	infall_rate=infall2(gmass,gcmass,time)[1] 		
	acc_rate=infall_rate*gcmass 		
	NSC_mass=init_mass+acc_rate*time
	m=np.random.normal(np.log10(NSC_mass),scale=0.15)
	NSC_mass=10**m 	
	
	### Half-Mass Radius ###
	rhm=halfmass_rad(gal_type,NSC_mass)[0] 	

	### Density###
	density=(3. * NSC_mass)/(4. * np.pi * rhm**3)
	
	### Escape Velocity ###
	G,msun,pc=6.67E-11,2.E30,3.086E16
	v_esc=np.sqrt(0.4*NSC_mass * msun *G/(rhm * pc)) * 100
	disp=v_esc/2
	
	return init_mass, NSC_mass, rhm, v_esc, density#,disp

"""
mgc=5e5
time=np.linspace(1,10000,10000)
v=nsc_prop(cl.gal_type,m,mgc,time)[3]
plt.plot(time,v)
plt.xscale('log')
plt.yscale('log')
plt.show()
#"""
