import numpy as np
import matplotlib.pyplot as plt
import classes
import infall2 
cl=classes.initial()

mg=cl.gmass
mgc=cl.gcmass

N=100
tburn=np.zeros(len(mg),float)
Rg=np.zeros(len(mg),float)
Rgc=np.zeros((N,len(mg)),float)
#Nc=np.zeros((N,len(mg),len(mgc)),float)
MSC=np.zeros(len(mg),float)
MSC1=np.zeros((3,len(mg)),float)
#for i in range(len(mg)):
r1,r2=infall2.infall(cl)
tburn=infall2.timescales(cl)[1]
trep=infall2.timescales(cl)[0]
#tdf=infall2.timescales(cl)[2]
Rg=infall2.radius(cl)[0]
N=infall2.number(cl,Rg,Rg)
#print(tburn,tdf)
plt.loglog(mg,trep,label='Replenishment Time')#,color=np.log10(N))
#plt.loglog(mg,tdf,label='tdf')
#plt.loglog(mg,tburn,label='tburn')
plt.scatter(mg,tburn,label='Burning Time',c=np.log10(N),marker="s")
plt.legend()
plt.xlabel('Galaxy Mass [M$_\odot$]')
plt.ylabel('Time $[MYr]$')
plt.title('Timescales')
plt.colorbar(label="$log(N_{GC})$")
plt.savefig("timescales.png",dpi=300.)
plt.show()
ma=r2*mgc
mi=infall2.mass(cl)
MSC=ma*tburn+mi
vel=infall2.escape_vel(cl,MSC)
MSC1[:]=infall2.scale_mass(cl,mg)
for i in range(len(mg)):
	if MSC1[1,i]>MSC1[2,i]:
		MSC1[1,i],MSC1[2,i]=MSC1[2,i],MSC1[1,i]
error=[MSC1[1,:],MSC1[2,:]]
plt.loglog(mg,mi,label="Initial Mass",color="black",linestyle="-.")
plt.loglog(mg,MSC,label="NSC Mass (Model)",color="black")
plt.scatter(mg,MSC1[0,:],color="black",marker="o")
plt.errorbar(mg,MSC1[0,:],yerr=error,label="NSC Mass (Scaling Relation)",color="red",ecolor="red", capsize=3,barsabove=True,fmt="o")
#plt.scatter(mg,MSC,color="black",marker="s")
plt.ylim(1e3,1e10)
plt.xlabel('Galaxy Mass [M$_\odot$]')
plt.ylabel('NSC Mass [M$_\odot$]')
#plt.title('')
plt.legend()
#plt.show()
#plt.loglog(mg,vel)
#plt.scatter(mg,vel)
#plt.xscale('log')
#plt.show()
plt.savefig('mass.png',dpi=300.)

"""
Rg=infall2.radius(cl)[0]
Rgc[:]=np.linspace(0,Rg,N)
#print("%.2e %.2e %.2e %.2e" %(mg[i],tburn[i],MSC[i],MSC1[0,i]))





for i in range(len(mg)):
#for j in range(len(mgc)):
	Nc[:,i,0]=number(cl,Rgc[:,i],Rg[i],i,0)
	plt.plot(Rgc[:,i],Nc[:,i,0], label=('%.2e' %mg[i]))
	plt.yscale('log')
plt.show()

#print(np.shape(cl.gmass),np.shape(cl.gcmass),np.shape(Nc[0,:,:]))
#ax=plt.axes(projection='3d')
#ax=plt.figure().add_subplot(projection='3d')
#ax.plot(cl.gmass,cl.gcmass,Nc[0,:,:])
#plt.show()
def t3bb(den,sigma,mass):
	feq=1.0
	t3bb=125.0*(1e6/den)**2  * (sigma/30.e5/feq)**9 * (20./mass)**5
	return t3bb
sigma=vel/(2*1.73)#np.logspace(6,8,10)
t=t3bb(1e6,sigma,20)
plt.loglog(mg,t)
plt.scatter(mg,t, c=np.log(MSC))
plt.show()
"""	
	
	
	
