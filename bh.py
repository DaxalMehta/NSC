import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib
import matplotlib as mpl


bg_color= "#1e1f24"
ft_color= "#f1c232"
ft_color1= "#0092cc"#"#ffab40"


matplotlib.rc('axes',edgecolor=ft_color) 
plt.rcParams['image.cmap'] = 'YlOrBr'
mpl.rcParams['text.color'] = ft_color
mpl.rcParams['axes.labelcolor'] = ft_color
mpl.rcParams['xtick.color'] = ft_color
mpl.rcParams['ytick.color'] = ft_color

#dire="../rapid_A1_NSC_chi01_output_noclusterevolv/Dyn/0.02/"
#dire="../output_noclusterevolv/Dyn/0.02/"
dire="../2_2_23/clusterevolv_1e9/Dyn/0.02/"
idi,gen=np.genfromtxt(dire+"nth_generation.txt",usecols=(0,27),unpack=True,skip_header=1)
idi0=np.genfromtxt(dire+"first_gen.txt",usecols=(0),unpack=True,skip_header=1)
m1,tdf,mmerg,tmerg=np.genfromtxt(dire+"nth_generation.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
m10,tdf0,mmerg0,tmerg0=np.genfromtxt(dire+"first_gen.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
tform=np.genfromtxt(dire+"timescales_1stgen.txt", usecols=4,unpack=True,skip_header=1)

#dire1="../rapid_A1_NSC_chi01_output_clusterevolv1/Dyn/0.02/"
dire1="../2_2_23/clusterevolv_1e10/Dyn/0.02/"
idie,gene=np.genfromtxt(dire1+"nth_generation.txt",usecols=(0,27),unpack=True,skip_header=1)
idi0e=np.genfromtxt(dire1+"first_gen.txt",usecols=(0),unpack=True,skip_header=1)
m1e,tdfe,mmerge,tmerge=np.genfromtxt(dire1+"nth_generation.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
m10e,tdf0e,mmerg0e,tmerg0e=np.genfromtxt(dire1+"first_gen.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
tforme=np.genfromtxt(dire1+"timescales_1stgen.txt", usecols=4,unpack=True,skip_header=1)
"""
L=max(gen)
g=np.arange(0,L,1)
print(L)
M=[]
num=[]
num.append(1)
M.append(max(mmerg0))
for i in range(2,int(L+1)):
	bb=np.where(gen==i)
	b=bb[0]
	num.append(len(b)/len(m10))
	mass=mmerg[bb]
	M.append(max(mass))
Le=max(gene)
print(Le)
ge=np.arange(0,Le,1)
Me=[]
nume=[]
nume.append(1)
Me.append(max(mmerg0e))
for i in range(2,int(Le+1)):
	bbe=np.where(gene==i)
	be=bbe[0]
	nume.append(len(be)/len(m10e))
	masse=mmerge[bbe]
	Me.append(max(masse))
fig,axs=plt.subplots(2,2)
fig.suptitle('No Evolve                                                     Evolve')
axs[0,0].plot(M)
axs[0,0].scatter(g,M)
axs[0,0].set_xlabel('Generation')
axs[0,0].set_ylabel('Maximum Mass')
axs[0,1].plot(Me)
axs[0,1].scatter(ge,Me)
axs[0,1].set_xlabel('Generation')
axs[0,1].set_ylabel('Maximum Mass')
axs[1,0].plot(num)
axs[1,0].scatter(g,num)
axs[1,0].set_xlabel('Generation')
axs[1,0].set_ylabel('Fraction')
axs[1,1].plot(nume)
axs[1,1].scatter(ge,nume)
axs[1,1].set_xlabel('Generation')
axs[1,1].set_ylabel('Fraction')
plt.show()
"""
#"""
fig,axs=plt.subplots(2,2,figsize=(18,12),facecolor=bg_color)
axs[0,0].set_facecolor(bg_color)
axs[0,1].set_facecolor(bg_color)
axs[1,0].set_facecolor(bg_color)
axs[1,1].set_facecolor(bg_color)
cm = plt.cm.get_cmap('YlOrBr')
colors= cm(np.linspace(0,1,20))
end=np.where(gen==2)
b_end=idi[end]
a=0
#print(b_end)
for i in range(len(b_end)):
	if a>19:a=0
	m=[]
	t=[]
	n=np.where(idi0==b_end[i])[0]
	#t=[0.0]
	for j in range(len(n)):
		m.append(m10[n[j]])
		m.append(mmerg0[n[j]])
		t.append(tform[n[j]])
		t.append(tmerg0[n[j]])	
	n=np.where(idi==b_end[i])[0]
	for j in range(len(n)):
		m.append(mmerg[n[j]])
		t.append(tmerg[n[j]])
	#print(m)
	if np.all(m)==False: 
		m=[]
		t=[]
	ran=np.random.choice((0,1))
	ran1=np.random.choice((0,1))
	if ran==1:
		if ran1==1:	
			axs[0,0].scatter(t,m,color=colors[a])
			axs[0,0].plot(t,m,color=colors[a])
			#axs[0,0].set_xlabel("$t_\mathrm{merg}$ [Myr]", fontsize=14)
			axs[0,0].set_ylabel("Mass [M$_\odot$]", fontsize=20)
			#axs[0,0].set_title("$10^{9}$ [M$_\odot$].")
			a+=1
axs[0,0].scatter(t,m,color=colors[a],label="$M_{gal}=10^{9}$ [M$_\odot$]")
axs[0,0].plot(t,m,color=colors[a])
axs[0,0].legend(markerscale=0,fontsize=20,loc="upper right",facecolor=bg_color)
ende=np.where(gene==3)
b_ende=idie[ende]
a=0
for i in range(len(b_ende)):
	if a>19:a=0
	m=[]
	#t=[tforme[b_ende[i]]]
	t=[]
	n=np.where(idi0e==b_ende[i])[0]
	for j in range(len(n)):
		m.append(m10e[n[j]])
		m.append(mmerg0e[n[j]])
		t.append(tforme[n[j]])
		t.append(tmerg0e[n[j]])	
	n=np.where(idie==b_ende[i])[0]
	for j in range(len(n)):
		m.append(mmerge[n[j]])
		t.append(tmerge[n[j]])
	#print(m)
	if np.all(m)==False: 
		m=[]
		t=[]
	ran=np.random.choice((0,1))
	ran1=np.random.choice((0,1))
	if ran==1:
		if ran1==1:	
			axs[0,1].scatter(t,m,color=colors[a])
			axs[0,1].plot(t,m,color=colors[a])
			#axs[0,1].set_xlabel("$t_\mathrm{merg}$ [Myr]", fontsize=14)
			#axs[0,1].set_ylabel("Mass [M$_\odot$]", fontsize=14)
			#axs[0,1].set_title("$10^{10}$ [M$_\odot$].")
			a+=1
axs[0,1].scatter(t,m,color=colors[a],label="$M_{gal}=10^{10}$ [M$_\odot$]")
axs[0,1].plot(t,m,color=colors[a])
axs[0,1].legend(markerscale=0,fontsize=20,loc="upper right",facecolor=bg_color)
dire="../2_2_23/clusterevolv_1e11/Dyn/0.02/"
idi,gen=np.genfromtxt(dire+"nth_generation.txt",usecols=(0,27),unpack=True,skip_header=1)
idi0=np.genfromtxt(dire+"first_gen.txt",usecols=(0),unpack=True,skip_header=1)
m1,tdf,mmerg,tmerg=np.genfromtxt(dire+"nth_generation.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
m10,tdf0,mmerg0,tmerg0=np.genfromtxt(dire+"first_gen.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
tform=np.genfromtxt(dire+"timescales_1stgen.txt", usecols=4,unpack=True,skip_header=1)

#dire1="../rapid_A1_NSC_chi01_output_clusterevolv1/Dyn/0.02/"
dire1="../2_2_23/clusterevolv_1e12/Dyn/0.02/"
idie,gene=np.genfromtxt(dire1+"nth_generation.txt",usecols=(0,27),unpack=True,skip_header=1)
idi0e=np.genfromtxt(dire1+"first_gen.txt",usecols=(0),unpack=True,skip_header=1)
m1e,tdfe,mmerge,tmerge=np.genfromtxt(dire1+"nth_generation.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
m10e,tdf0e,mmerg0e,tmerg0e=np.genfromtxt(dire1+"first_gen.txt",usecols=(1,8,15,13),unpack=True,skip_header=1)
tforme=np.genfromtxt(dire1+"timescales_1stgen.txt", usecols=4,unpack=True,skip_header=1)
end=np.where(gen==4)
b_end=idi[end]
a=0
#print(b_end)
for i in range(len(b_end)):
	if a>19:a=0
	m=[]
	t=[]
	n=np.where(idi0==b_end[i])[0]
	#t=[0.0]
	for j in range(len(n)):
		m.append(m10[n[j]])
		m.append(mmerg0[n[j]])
		t.append(tform[n[j]])
		t.append(tmerg0[n[j]])	
	n=np.where(idi==b_end[i])[0]
	for j in range(len(n)):
		m.append(mmerg[n[j]])
		t.append(tmerg[n[j]])
	#print(m)
	if np.all(m)==False: 
		m=[]
		t=[]
	ran=np.random.choice((0,1))
	ran1=np.random.choice((0,1))
	if ran==1:
		if ran1==1:	
			axs[1,0].scatter(t,m,color=colors[a])
			axs[1,0].plot(t,m,color=colors[a])
			axs[1,0].set_xlabel("$t_\mathrm{merg}$ [Myr]", fontsize=20)
			axs[1,0].set_ylabel("Mass [M$_\odot$]", fontsize=20)
			#axs[1,0].set_title("$10^{11}$ [M$_\odot$].")
			a+=1
axs[1,0].scatter(t,m,color=colors[a],label="$M_{gal}=10^{11}$ [M$_\odot$]")
axs[1,0].plot(t,m,color=colors[a])
axs[1,0].legend(markerscale=0,fontsize=20,loc="upper right",facecolor=bg_color)

ende=np.where(gene==2)
b_ende=idie[ende]
a=0
for i in range(len(b_ende)):
	if a>20:a=0
	m=[]
	#t=[tforme[b_ende[i]]]
	t=[]
	n=np.where(idi0e==b_ende[i])[0]
	for j in range(len(n)):
		m.append(m10e[n[j]])
		m.append(mmerg0e[n[j]])
		t.append(tforme[n[j]])
		t.append(tmerg0e[n[j]])	
	n=np.where(idie==b_ende[i])[0]
	for j in range(len(n)):
		m.append(mmerge[n[j]])
		t.append(tmerge[n[j]])
	#print(m)
	if np.all(m)==False: 
		m=[]
		t=[]
	ran=np.random.choice((0,1))
	ran1=np.random.choice((0,1))
	if ran==1:
		if ran1==1:	
			axs[1,1].scatter(t,m,color=colors[a])
			axs[1,1].plot(t,m,color=colors[a])
			axs[1,1].set_xlabel("$t_\mathrm{merg}$ [Myr]", fontsize=20)
			#axs[1,1].set_ylabel("Mass [M$_\odot$]", fontsize=14)
			#axs[1,1].set_title("$10^{12}$ [M$_\odot$].")
			#axs[1,1].legend("$10^{12}$ [M$_\odot$].")
			a+=1
axs[1,1].scatter(t,m,color=colors[a],label="$M_{gal}=10^{12}$ [M$_\odot$]")
axs[1,1].plot(t,m,color=colors[a])
axs[1,1].legend(markerscale=0,fontsize=20,loc="upper right",facecolor=bg_color)

#plt.show()
plt.savefig("Merger_chain.png",dpi=300)
#"""
"""
end=np.where(gen==3)
ende=np.where(gene==3)
tt=tform[end]
tte=tforme[ende]
fig,axs=plt.subplots(1,2)
fig.suptitle("Distribution of tform")
axs[0].hist(tt,bins=50)
axs[0].set_xlabel("Distribution")
axs[0].set_ylabel("$t_form$")
axs[0].set_title('$10^9$')
axs[1].hist(tte,bins=50)
axs[1].set_xlabel("Distribution")
axs[1].set_ylabel("$t_{form}$")
axs[1].set_title('$10^{10}$')
plt.show()
"""
"""
end=np.where(gen==2)
ende=np.where(gene==2)
mm=m10[end]
mme=m10e[ende]
mm=np.concatenate((mm,m1))
mme=np.concatenate((mme,m1e))
fig,axs=plt.subplots(1,2)
fig.suptitle("Distribution of merger masses")
axs[0].hist(mm,bins=500)
axs[0].set_xlabel("Distribution")
axs[0].set_ylabel("$Mass$")
axs[0].set_title('No Evolve')
axs[1].hist(mme,bins=500)
axs[1].set_xlabel("Distribution")
axs[1].set_ylabel("$Mass$")
axs[1].set_title('Evolve')
plt.show()
#"""
"""
end=np.where(gen==2)
ende=np.where(gene==2)
m10=m10[end]
m10e=m10e[ende]
tdf0=tdf0[end]
tdf0e=tdf0e[ende]
t=np.concatenate((tdf0,tdf))
mm=np.concatenate((m10,m1))
plt.scatter(mm,t)
plt.show()
mme=np.concatenate((m10e,m1e))
te=np.concatenate((tdf0e,tdfe))
plt.scatter(mme,te)
plt.show()
#plt.hist(t)
#plt.hist(te)
#plt.show()
plt.hist(mm,bins=50)
plt.hist(mme,bins=50)
plt.show()
"""

