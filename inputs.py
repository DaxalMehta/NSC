import numpy as np
####Galaxy Properties####
galaxy_type="Late" #Early or Late
gmass=1e8,3e8,1e9,3e9,1e10,3e10,1e11,3e11,1e12,3e12#1e6,3e6,1e7,3e7,
slope=1.2
k=0.14
alpha=-0.67
beta=1.76
r=0.01		#in Kpc
den=slope #np.random.uniform(0,0.5) #Density profile slope
eccen=0.5

GC_mass=5.E5#,1.E3,3.E3,5.E3,1.E4,3.E4,5.E4,1.E5,3.E5,5.E5,1.E6
NSC_mass=5.E6

time=1E3
