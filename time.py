import numpy as np
import matplotlib.pyplot as plt
import random
import imageio
from PIL import Image
import classes
import infall3
#"""
cl=classes.initial()
m=5e10
#m=np.array([1e8,3e8,1e9,3e9,1e10,3e10,1e11,3e11,1e12,3e12])
Rg=infall3.radius(m)[0]
num=infall3.number(m,5e5,Rg)
t,tt=infall3.timescales(m,5e5)[1:]
print("Galaxy Mass: ",m)
print("Scale Length: ",Rg)
print("Number: ",num)
print("tburn: ",t)
print("tdf: ",tt)
rgc=np.linspace(0.001,Rg*2,5000)
time=np.linspace(0.1,t*2,5000)
mi=infall3.mass(m)
msc=mi+infall3.infall2(m,5e5,time)[0]*5e5*time
msc2=mi+infall3.infall(m,5e5)[0]*5e5*time
plt.plot(time,msc,label="Old rate")
plt.plot(time,msc2,label="Updated rate")
plt.xlabel('Time (Myr)')
plt.ylabel('NSC Mass [M$_\odot$]')
plt.legend()
#plt.title('')
#plt.yscale('log')
#plt.xscale('log')

#tdf=t*(rgc/Rg)**1.76
#tdf2=infall3.timescales2(m,5e5,rgc)[2]
#plt.plot(rgc,tdf)
#plt.plot(rgc,tdf2)
plt.savefig('Mass Accretion.png', dpi=300)
trep=infall3.timescales2(m,5e5,rgc)[0]
plt.plot(rgc,trep)
plt.show()
#print(max(msc),msc2)
#"""




#### Check the t121 time #####

"""
def t12(m1,maverage,sma,sigma,cdensity,binfrac):
	#sma=sma/1.496e+11
	t12=3.0e9 * (0.01/binfrac) * (1.0e6/cdensity) * (sigma/50.e5) * (12./(2.*maverage+m1)) * (1./sma)
	t12=t12/1e6
	return t12

den=np.logspace(5,6,1000)
sigma=np.logspace(5,7,1000)
t=np.zeros((len(den),len(sigma)),float)
for i in range(len(den)):
	for j in range(len(sigma)):
		t[i,j]=t12(10,1,1,sigma[j],den[i],0.01)

plt.hist2d(t)
plt.show()
"""
#### Comparing the velocities and properties coming from the code #####
"""

#m=np.logspace(8,12,10)
m=[1e8,3e8,1e9,3e9,1e10,3e10,1e11,3e11,1e12,3e12]
#Rg=infall3.radius(m)[0]
msc=[]
msc1=[]
mm=[]
for i in range(len(m)):
#time=np.arange(1,10001,1)
	t=infall3.timescales(m[i],5e5)[1]#np.linspace(1,infall3.timescales(m[i],5e5)[1],10000)
	Mass,r,den,v_esc,disp=infall3.nsc_prop(cl.gal_type,m[i],2e5,t)
	mi=infall3.mass(m[i])
	#msc.append(v_esc)
	#v=40.0E5 * (Mass/1.E5)**(1./3.) * (den/1.E5)**(1./6.)
	#msc1.append(v)
	#mm.append(Mass)
	#msc=mi+infall3.infall2(m[i],5e5,t)[1]*5e5*t
	print("%.2e %.2e %.2e %.2e %.2e" %(m[i],mi,Mass,mi/Mass,t))
	
plt.scatter(m,msc,s=0.5)#,label='New Code')
#plt.scatter(m,msc,s=0.5)#,label='New Code')
#plt.scatter(mm,msc1,s=0.5,label='Old Fastcluster code')
plt.legend()
plt.xlabel('Galaxy_Mass $(Msun)$')
plt.ylabel('Velocity $(cm/s)$')
#plt.plot(t,msc)


mm=infall3.halfmass_rad(cl.gal_type,m)[0]
den=(3. * m)/(4. * np.pi * mm**3)
G,msun,pc=6.67E-11,2.E30,3.086E16
v_esc=np.sqrt(msc * msun *G/(mm * pc)) * 100

r=np.linspace(0,Rg,1000)
n=infall3.number(m,5e5,r)
plt.plot(r,n)

plt.xscale('log')
plt.yscale('log')
#plt.xlim(0,infall3.timescales(m,5e5)[1]+100)
plt.show()
"""

### Creating a 3d simulation of the accretion ####
"""
m=5e11
t=infall3.timescales(m,5e5)[1]
#tt=infall3.timescales(m,5e5)[0]
Rg=infall3.radius(m)[0]
num=infall3.number(m,5e5,Rg)
num=int(num)
def func(num,rgc,time):
	x=[]
	y=[]
	z=[]
	n=[]
	t=[]
	R=[]
	a=0
	for i in range(len(num)):
		r=rgc[i]#np.random.uniform(0,Rg)
		N=int(num[i])-a
		n.append(N)
		b=0
		for j in range(N):
			theta, phi = np.random.rand(2)*np.array([np.pi, 2*np.pi])
			xx = r * np.sin(theta) * np.cos(phi)
			yy = r * np.sin(theta) * np.sin(phi)
			zz = r * np.cos(theta)
			x.append(xx)
			y.append(yy)
			z.append(zz)
			t.append(time[i])
			R.append(r)
			b+=1
		a+=b
	return x,y,z,n,R,t

#t=infall3.timescales(m,5e5)[1]
#print(Rg,t)
rgc=np.linspace(0.1,Rg,1000)
time=t*(rgc/Rg)**1.76
#beta=1.76
#rgc=Rg*(time/t)**(1/beta)
#tdf=infall3.timescales2(m,5e5,rgc)[1]
#print(tdf[:10])
#rgc=np.linspace(0.1,Rg,1000)
num=infall3.number(m,5e5,rgc)

x,y,z,n,R,tim1=func(num,rgc,time)
x,y,z,R=np.array(x),np.array(y),np.array(z),np.array(R)
x1,y1,z1=np.array([0,0,0])
tdf=np.array(tim1)
frames=[]
timer=0
s=0
ss=len(x)
xx=x

while len(xx)>1:
	ok=np.where(tdf>timer)[0]
	xx=x[ok]
	yy=y[ok]
	zz=z[ok]
	RR=R[ok]
	fig=plt.figure(figsize=(12,10))
	ax=fig.add_subplot(111,projection='3d')
	d=ax.scatter(xx,yy,zz,c=RR)#,label='Time:'+str(timer)+' Myr and Number:'+str(len(xx)))
	ax.scatter(x1,y1,z1,s=(ss-len(xx))/5,c='black')
	#plt.scatter(xx,zz,c=tre)
	ax.set_xlim(-Rg,Rg)
	ax.set_ylim(-Rg,Rg)
	ax.set_zlim(-Rg,Rg)
	ax.grid(False)
	ax.set_xticks([])
	ax.set_yticks([])
	ax.set_zticks([])
	ax.set_title('Time:'+str(timer)+' Myr and Number:'+str(len(xx)))
	#ax.legend()
	#ax.view_init(elev=np.random.choice((-90,90,0)),azim=np.random.choice((-90,90,0)))
	#plt.colorbar()
	plt.draw()
	plt.axis('off')
	#fig.colorbar(d)
	timer+=5e1
	s+=1
	#print(ok)
	#frame = np.frombuffer(c.canvas.tostring_rgb(), dtype='uint8')
	#frame = frame.reshape(c.canvas.get_width_height()[::-1] + (3,))
	#frames.append(frame)
	name=str(s)+'.png'
	plt.savefig(name,dpi=75)
	new_frame = Image.open(name)
	frames.append(new_frame)
	plt.close()
	#new_frame.close()
	
#png_count = s
#files = []
#for i in range(png_count):
#    seq = str(i+1)
#    file_names = seq +'.png'
#    files.append(file_names)
    
#print(files)
#frames = []
#files
#for i in files:
#    new_frame = Image.open(i)
#    frames.append(new_frame)

# Save into a GIF file that loops forever   
frames[0].save('ani.gif', format='GIF',append_images=frames[1:],save_all=True)#,duration=40)#, loop=0)
#	x,y,z,trep=xx,yy,zz,tre
#imageio.mimsave('animation.mp4', frames)	
#ax.mouse_init()
#ax.dist=1
"""  	
#### Creating a histogram for the infall times of globular clusters ####
"""


N=int(1.e5)
fil="single_BH_0.02.txt"
tformii=np.genfromtxt(fil,usecols=(3),skip_header=3,unpack=True)
wrong=np.where(tformii<100.)
ok=wrong[0]
tformi=tformii[ok]
a=np.random.randint(0,len(tformi),N)
tform=tformi[a]
tform2=np.copy(tform)
tform3=np.copy(tform)
tform4=np.copy(tform)

for i in range(N-int(N*frac)):
	tform[i]+=np.random.uniform(0,t)

#plt.hist(tform,bins=100)
#plt.show()

#for i in range(N):
#	r=np.random.random()
#	if r>frac:
#		tform2[i]+=np.random.uniform(0,t)	

#plt.hist(tform2,bins=100)
#plt.show()

for i in range(N):
	r=np.random.random()
	if r>frac:
		s=np.random.random()
		Num=int((1-s)*num)
		tform3[i]+=np.random.uniform(Num*tt,(Num+1)*tt)


#plt.hist(tform3,bins=100)
#plt.show()

#time=np.arange(1,t,1)
beta=1.76
#rgc=Rg*(time/t)**(1/beta)
#num=infall3.number(m,5e5,rgc)
#plt.plot(time,rgc)
#plt.show()
#num=infall3.number(m,5e5,rgc)
#for i in range(len(num)):
#	num[i]=round(num[i],2)
#num=num.astype(int)

#plt.plot(time,num)
#plt.show()
#tburn=infall3.infall2(m,5e5,time)[1]
#trep=infall3.infall2(m,5e5,time)[1]
#plt.plot(num,trep)
#plt.show()
#plt.plot(time,tburn)
#plt.show()
#print(num[:10])
#print(trep[:10])
#print(x[:10])
#print(trep[x])
m=5e10
mi=infall3.mass(m)
t=infall3.timescales(m,5e5)[1]
Rg=infall3.radius(m)[0]
msc=mi+infall3.infall(m,5e5)[1]*t*5e5
print("%.2e %.2e %.2e" %(Rg,mi,msc))
frac=mi/msc
print(frac)
for i in range(N):
	r=np.random.random()
	if r>frac:
		r=np.random.uniform(0.001,Rg)
		#s=np.random.random()
		#Num=int((1-s)*(len(num)-1))
		#print(len(np.where(num==Num)[0]))
		#t1=trep[np.where(num==Num)[0]]
		#t2=trep[np.where(num==(Num+1))[0]]
		tform4[i]+=t*(r/Rg)**beta#np.random.uniform(Num*trep[Num],(Num+1)*trep[Num+1])
plt.hist(tform4,bins=100)
plt.show()
"""



