import numpy as np
import matplotlib.pyplot as plt
import classes 
from matplotlib import colors
from astropy import constants as const
from astropy import units as u
cl=classes.initial()

def number(cl,Rgc,Rg):
	N=0.01*(cl.gmass/cl.gcmass)*(Rgc/(Rgc+Rg))**(3-cl.slope)
	return N

def radius(cl):
	mg=cl.gmass/1.E11
	c1=2.**(1./(3.-cl.slope))-1.
	Rg=2.37 * c1 * (mg)**cl.k
	R=31.26 * Rg * mg**0.167
	return Rg,R

def density(cl):
	mg=cl.gmass/1.E11
	Rg,R=radius(cl)
#	Rg=cl.Rg
	rho0=(3.-cl.den) * mg/(4. * np.pi * Rg**3.)
	rho=rho0/((cl.r/Rg)**cl.den * ((cl.r/Rg + 1.)**(4.-cl.den)))
	return rho0,rho	

def mass(cl):
	Rg,R=radius(cl)
#	Rg=cl.Rg
	mass = (cl.gmass * (cl.r/(cl.r + Rg))**(3.0-cl.den))/(3.-cl.den)
	return mass

def infall(cl):
	mg=cl.gmass/1.E11
	mgc=cl.gcmass/1.E11
	a1 = 2.63
	a2 = 2.26
	a3 = 0.9        
	gfact=(2.-cl.slope) * ((a1 * pow(1./(2.-cl.slope), a2) + a3) * (1.-cl.eccen) + cl.eccen)
	f=0.3 * gfact * pow(2.37,1.5)*(2.**(1./(3.-cl.slope))-1.0)**(1.5)
	rateg=0.01*2.0**(-3. + cl.den)/f * mg**(1.5*(1.-cl.k) + cl.alpha) * mgc**(-1.-cl.alpha)
	rateh=(2.0**(2.0-cl.slope)) * (2.0**(1.0/(3.0-cl.slope))-1.0)**cl.beta*rateg
	return rateg,rateh


def extract(cl):
	mu = pow(np.random.random(), 1./(3.-cl.slope))
	Rg,R=radius(cl)
#	Rg=cl.Rg
	pos = Rg * mu/(1.-mu)
	return pos

def timescales(cl):
	Rg,r=radius(cl)
#	Rg=cl.Rg
	rgc=extract(cl)        #the extraction must to be done according to the cluster density profile
	rateg,rateh=infall(cl)
	time=rateg**-1.0
	
	mg=cl.gmass/1.E11
	mgc=cl.gcmass/1.E11
	
	#tburn=1.25e-2*2.0**cl.slope*mg*(mgc*rateg)**-1.0  ## a minus in the exponent is missing in the paper
	tburn =time * mg/mgc * 0.01 * pow(2.,-3.+cl.den)
	#print(tburn)
	
	#tdf=0.3*(2-cl.slope)*(4.93-3.93*cl.eccen)*(rg**3/mg1)**0.5*(mgc1/mg1)**cl.alpha*(rg/rg)**cl.beta
	#actual df time from Arca Sedda et al 2015
	a1 = 2.63
	a2 = 2.26
	a3 = 0.9
	rrh= Rg/(pow(2.,1./(3.-cl.slope))-1.)
	gfact = (2-cl.slope)*((a1*pow(1./(2.-cl.slope), a2) + a3)*(1.-cl.eccen) + cl.eccen)
	tdf   = 0.3*(Rg**3/mg)**0.5* gfact * (mgc/mg)**cl.alpha * (Rg/Rg)**cl.beta

#	print("warning: ",rg, mg1, mgc1, cl.beta, cl.alpha)
	return time,tburn,tdf


def escape_vel(cl,mass):
	rho=density(cl)[1]*1.E2
#	rho=cl.rho*1.E2
	vesc=40.0E5 * (mass/1.E5)**(1./3.) * (mass/1.E8)**(1./6.)
	#vesc=40.0E5 * (mass/1.E5)**(1./3.) * (rho/1.E5)**(1./6.)
	return vesc

def halfmass_rad(cl,mass):
	if cl.gal_type=="Early":
		c1,c2=6.27,1.95E6
		alpha,beta=0.347,-0.024
		reff=c1 * 10**(alpha * np.log10(mass/c2) + beta)
	elif cl.gal_type=="Late":
		c1,c2=3.31,3.65E6
		alpha,beta=0.321,-0.011
		reff=c1 * 10.**(alpha * np.log10(mass/c2) + beta)
	rhm=4/3*reff
	return rhm,reff
def halfmass_rad2(cl,mass):
	if cl.gal_type=="Early":
		c1,c2=6.11,2.09E9
		alpha,beta=0.326,-0.011
		reff=c1 * 10**(alpha * np.log10(mass/c2) + beta)
	elif cl.gal_type=="Late":
		c1,c2=3.44,5.61E9
		alpha,beta=0.356,-0.012
		reff=c1 * 10.**(alpha * np.log10(mass/c2) + beta)
	rhm=4/3*reff
	return rhm,reff

def scale_mass(cl,mass):
	if cl.gal_type=="Early":
		c1,c2=2.24E6,1.755E9
		alpha,beta=1.363,0.010
		reff=c1 * 10**(alpha * np.log10(mass/c2) + beta)
	elif cl.gal_type=="Late":
		c1,c2=2.78E6,3.94E9
		alpha,beta=1.001,0.016
		reff=c1 * 10.**(alpha * np.log10(mass/c2) + beta)
		alpha,beta=1.001-0.067,0.016-0.061
		reff1=c1 * 10.**(alpha * np.log10(mass/c2) + beta)
		alpha,beta=1.001+0.054,0.016+0.023
		reff2=c1 * 10.**(alpha * np.log10(mass/c2) + beta)
	#rhm=4/3*reff
	return reff,reff1,reff2

def scale_den(cl,mass):
	rhm=halfmass_rad(cl,mass)[0]
	den=(3. * mass)/(4. * np.pi * rhm**3)
	return den

def nsc_prop(cl,time):
	init_mass=mass(cl)  			# MSun
#	init_mass=cl.mass
	infall_rate=infall(cl)[1] 		# 1/Myr
	acc_rate=infall_rate*cl.gcmass 		#Msun/Myr
	NSC_mass=init_mass+acc_rate*time 	#Msun
	rhm=halfmass_rad(cl,NSC_mass)[0] 	#pc
	density=NSC_mass/1000. 			#Msun/pc3
	scale_d=scale_den(cl,NSC_mass) 		#Msun/pc3
	esc_vel=escape_vel(cl,NSC_mass) 	#cm/s
	return NSC_mass,rhm,density,esc_vel,scale_d
#for i in range(len(cl.gmass)):
#	tburn=timescales(cl,i)[1]
#	print(tburn)
"""
N=len(cl.gmass)
if N==0:N=1
M=len(cl.age)
MSC=np.zeros((N,M),float)
Vesc=np.zeros((N,M),float)
rhm=np.zeros((N,M),float)
den=np.zeros((N,M),float)
time=np.zeros(N,float)
tburn=np.zeros(N,float)
tdf=np.zeros(N,float)
for i in range(N):
	r1,r2=infall(cl,i)
	Rg,R=radius(cl,i)
	time[i],tburn[i],tdf[i]=timescales(cl,i)
	mg=cl.gmass[i]
	mgc=cl.gcmass
	ma=r2*mgc
	mi=mass(cl,i)
	MSC[i,:]=ma*cl.age+mi
	Vesc[i,:]=escape_vel(cl,MSC[i,:],i)
	rhm[i,:]=halfmass_rad(cl,MSC[i,:])[0]
	den[i,:]=scale_den(cl,MSC[i,:])

	print("Galaxy Mass: %.2e Msun" %cl.gmass[i])
	print("GC Mass: %.2e Msun" %cl.gcmass)
	print("Radius under which all GC mergers into NSC within th: %.2e Kpc" %Rg)
	print("Infall Rate h: %.2e 1/Myr" %r2)
	print("Infall Rate g: %.2e 1/Myr" %r1)
	print("Replenishment time : %.2e Myr" %time[i])
	print("Mass accretion rate: %.2e Msun/Myr" %ma)
	print("Mass inside 10pc initial and end: [%.2e %.2e] Msun" %(mi, MSC[i,M-1]))
	print("Central Density initial and end: [%.2e %.2e] Msun/pc3 " %(density(cl,i)[1]*100., MSC[i,M-1]/1000.))
	print("Central Density initial and end: [%.2e %.2e] Msun/pc3 " %(mi/1000., MSC[i,M-1]/1000.))
	print("Escape Velocity initial and end: [%.2e %.2e] cm/s" %(Vesc[i,0], Vesc[i,M-1]))
	print("Half mass radius initial and end: [%.2e %.2e] pc" %(rhm[i,0],rhm[i,M-1]))
	print("Scale Density initial and end: [%.2e %.2e] Msun/pc3" %(den[i,0],den[i,M-1]))
	print("#########################")
	"""
#	plt.plot(cl.age,MSC[i,:])
#	plt.xlabel('Time Myr')
#	plt.ylabel('Mass accreted Msun')
#	plt.plot(cl.age,Vesc[i,:])
#	plt.pause(1)
"""
print(tburn)
plt.plot(np.sqrt(MSC[N-1,:]),rhm[N-1,:])
plt.show()

mg=cl.gmass
plt.scatter(mg,time)
plt.yscale('log')
plt.ylabel('Time Myr')
plt.xscale('log')
plt.xlabel('Galaxy Mass')
plt.loglog(mg,tburn)
plt.loglog(mg,tdf)
plt.show()
"""


