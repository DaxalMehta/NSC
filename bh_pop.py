import numpy as np
import matplotlib.pyplot as plt
import infall3

m=5e10 # Mass of the galaxy
tt,t=infall3.timescales(m,5e5)[:2]	#t is the burning time or the dynamical friction time for the last GC. tt is the replenishment  time or time between two successive GC mergers.

Rg=infall3.radius(m)[0]
num=infall3.number(m,5e5,Rg) # Number of GC within Rg, i.e. Scale Length of the galaxy
print(num)
N=int(1.e5)
fil="single_BH_0.02.txt"
tformii=np.genfromtxt(fil,usecols=(3),skip_header=3,unpack=True)
wrong=np.where(tformii<100.)
ok=wrong[0]
tformi=tformii[ok]
a=np.random.randint(0,len(tformi),N)
tform=tformi[a]
tform1=np.copy(tform)
tform2=np.copy(tform)
frac=0.1 # This is the mass fraction of the initial and final NSC. I have put a numerical value for simplicity
#for i in range(N-int(N*frac)):
#	tform[i]+=np.random.uniform(0,t)
#"""
for i in range(N):
	r=np.random.random()
	if r>frac:
		s=np.random.random()
		Num=int((1-s)*num)
		tform1[i]+=np.random.uniform(Num*tt,(Num+1)*tt)

fig,ax=plt.subplots(1,2,figsize=(16,9))
ax[0].hist(tform1,bins=100, label="Old pop")
ax[0].set_xlabel('tform (Myr)')
ax[0].set_ylabel('Number')
ax[0].legend()
#ax[0].set_title('Randomly')
#"""
rgc=np.linspace(0.001,Rg,1000)
num=infall3.number(m,5e5,rgc)
num=num.astype(int)
trep=infall3.timescales2(m,5e5,rgc)[0]
for i in range(N):
	r=np.random.random()
	if r>frac:
		#r=np.random.uniform(0.001,Rg)
		s=np.random.random()
		Num=int((1-s)*(max(num)))
		ok=np.where(num==Num)[0]
		ok1=np.where(num==(Num+1))[0]
		#print(max(ok))
		t1=trep[ok]
		t2=trep[ok1]
		tform2[i]+=np.random.uniform(Num*np.mean(t1),(Num+1)*np.mean(t2)) #t*(r/Rg)**beta
ax[1].hist(tform2,bins=100,label="New pop")
ax[1].set_xlabel('tform (Myr)')
ax[1].set_ylabel('Number')
#ax[1].set_title('Statistically')
ax[1].legend()
plt.savefig('Population.png',dpi=300)
plt.close()



