import numpy as np

############ physical constants##################
tHubble = 13.6e3 #hubble time in Myr
parsec=3.08568e+18
msun=1.98892e33 #msun/g
AU=1.495978707e+13 #AU/cm
Rsun=6.95700e10 #rsun/cm
G=6.6743015e-8  #gravity constant cgs
c=2.99792458e10 #speed of light cgs
c5=c*c*c*c*c
G3=G*G*G
yr=3.1557600e7 #yr/seconds


############star cluster properties##################
c2hm_dens=20. #50. central density = c2hm_dens * half mass density
#to be improved



############black hole properties##################
#m2min=3.0 #min BH mass in Msun
#xi=1.0 #0.1-10 Hills 1983, Quinlan 1996, Sesana 2006
#ki=0.00 #0.01-0.1 Quinlan 1996, Sesana 2006
