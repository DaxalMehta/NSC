import numpy as np
import matplotlib.pyplot as plt
import random
import classes
import infall2 
cl=classes.initial()
#x=np.logspace(8,12,1000)
#def func(x):
#	y=
#	return y

N=10000
mg=[]
msc=[]
den=[]
vel=[]
for i in range(N):
	m=random.randrange(40000,80000,1)
	m=10**(m/10000)
	mg.append(m)
	mm=infall2.halfmass_rad(cl,m)[0]
	mm=np.random.normal(mm,scale=np.exp(-0.5*(mm-4)**2/(0.8)**2))
	msc.append(mm)
	d=(3. * m)/(4. * np.pi * mm**3)
	v=40.0E5 * (m/1.E5)**(1./3.) * (d/1.E5)**(1./6.)
	den.append(d)
	vel.append(v)
#print(n)
plt.scatter(mg,vel,s=1)
plt.xscale('log')
plt.yscale('log')
plt.show()
